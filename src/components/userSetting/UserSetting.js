import React from 'react';

import styles from './UserSetting.module.scss';

const UserSetting = () => {
    return (
        <div className={styles.userSetting}>
            <h3 className={styles.userSettingHeader}>Настройки профиля</h3>
            <div className={styles.userSettingInputsWrapper}>
                <div className={styles.userSettingInputContainer}>
                    <p className={styles.userSettingInputText}>Дата обмена:</p>
                    <div className={styles.userSettingInputWrap}>
                        <input className={styles.userSettingInput}
                        />
                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15" fill="none">
                            <g clipPath="url(#clip0_1_124)">
                                <path d="M8.26312 3.19559L11.8044 6.73688L3.54133 15H0V11.4587L8.26312 3.19559ZM9.44355 2.01514L11.2142 0.244475C11.5402 -0.0814918 12.0687 -0.0814918 12.3946 0.244475L14.7556 2.60537C15.0815 2.93133 15.0815 3.45984 14.7556 3.78581L12.9848 5.55648L9.44355 2.01514Z" fill="#1DA0FF"/>
                            </g>
                            <defs>
                                <clipPath id="clip0_1_124">
                                    <rect width="15" height="15" fill="white" transform="translate(15 15) rotate(-180)"/>
                                </clipPath>
                            </defs>
                        </svg>
                    </div>
                </div>
                <div className={styles.userSettingInputContainer}>
                    <p className={styles.userSettingInputText}>Логин:</p>
                    <div className={styles.userSettingInputWrap}>
                        <input className={styles.userSettingInput}
                        />
                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15" fill="none">
                            <g clipPath="url(#clip0_1_124)">
                                <path d="M8.26312 3.19559L11.8044 6.73688L3.54133 15H0V11.4587L8.26312 3.19559ZM9.44355 2.01514L11.2142 0.244475C11.5402 -0.0814918 12.0687 -0.0814918 12.3946 0.244475L14.7556 2.60537C15.0815 2.93133 15.0815 3.45984 14.7556 3.78581L12.9848 5.55648L9.44355 2.01514Z" fill="#1DA0FF"/>
                            </g>
                            <defs>
                                <clipPath id="clip0_1_124">
                                    <rect width="15" height="15" fill="white" transform="translate(15 15) rotate(-180)"/>
                                </clipPath>
                            </defs>
                        </svg>
                    </div>
                </div>
                <div className={styles.userSettingInputContainer}>
                    <p className={styles.userSettingInputText}>Пароль:</p>
                    <div className={styles.userSettingInputWrap}>
                        <input className={styles.userSettingInput}
                        />
                        <svg xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15" fill="none">
                            <g clipPath="url(#clip0_1_124)">
                                <path d="M8.26312 3.19559L11.8044 6.73688L3.54133 15H0V11.4587L8.26312 3.19559ZM9.44355 2.01514L11.2142 0.244475C11.5402 -0.0814918 12.0687 -0.0814918 12.3946 0.244475L14.7556 2.60537C15.0815 2.93133 15.0815 3.45984 14.7556 3.78581L12.9848 5.55648L9.44355 2.01514Z" fill="#1DA0FF"/>
                            </g>
                            <defs>
                                <clipPath id="clip0_1_124">
                                    <rect width="15" height="15" fill="white" transform="translate(15 15) rotate(-180)"/>
                                </clipPath>
                            </defs>
                        </svg>
                    </div>
                </div>
            </div>
            <div className={styles.userSettingToggleSettingsWrapper}>
                <button className={styles.userSettingToggle}>
                    <svg
                        className={styles.userSettingToggleSvg}
                        width="42" height="24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g filter="url(#filter0_d_1055_6646)">


                            <rect x="18" y="3" width="18" height="18" rx="9" fill="#F78D5F"/>
                        </g>
                        <defs>
                            <filter id="filter0_d_1055_6646" x="0" y="0" width="82" height="64" filterUnits="userSpaceOnUse"
                                    colorInterpolationFilters="sRGB">
                                <feFlood floodOpacity="0" result="BackgroundImageFix"/>
                                <feColorMatrix in="SourceAlpha" type="matrix"
                                               values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                                <feOffset dy="4"/>
                                <feGaussianBlur stdDeviation="10"/>
                                <feComposite in2="hardAlpha" operator="out"/>
                                <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.06 0"/>
                                <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_1055_6646"/>
                                <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_1055_6646"
                                         result="shape"/>
                            </filter>
                        </defs>
                    </svg>
                </button>
                <p className={styles.userSettingToggleSettingsText}>Активировать 2FA</p>
                <button className={styles.userSettingToggle}>
                    <svg
                        className={styles.userSettingToggleSvg}
                        width="42" height="24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g filter="url(#filter0_d_1055_6646)">


                            <rect x="18" y="3" width="18" height="18" rx="9" fill="#F78D5F"/>
                        </g>
                        <defs>
                            <filter id="filter0_d_1055_6646" x="0" y="0" width="82" height="64" filterUnits="userSpaceOnUse"
                                    colorInterpolationFilters="sRGB">
                                <feFlood floodOpacity="0" result="BackgroundImageFix"/>
                                <feColorMatrix in="SourceAlpha" type="matrix"
                                               values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                                <feOffset dy="4"/>
                                <feGaussianBlur stdDeviation="10"/>
                                <feComposite in2="hardAlpha" operator="out"/>
                                <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.06 0"/>
                                <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_1055_6646"/>
                                <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_1055_6646"
                                         result="shape"/>
                            </filter>
                        </defs>
                    </svg>
                </button>
                <p className={styles.userSettingToggleSettingsText}>Отключить логирование</p>
            </div>

        </div>
    );
};

export default UserSetting;
