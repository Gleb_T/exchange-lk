import React from 'react';

import styles from './DayMonitor.module.scss';

const MyComponent = () => {
    return (
        <div className={styles.dayMonitor}>
            <svg
                className={styles.dayMonitorProcessBarFirstElement}
                xmlns="http://www.w3.org/2000/svg" width="140" height="140" viewBox="0 0 140 140" fill="none">
                <g filter="url(#filter0_iiii_1_165)">
                    <circle cx="70" cy="70" r="70" fill="white"/>
                </g>
                <defs>
                    <filter id="filter0_iiii_1_165" x="-2" y="-2" width="144" height="144" filterUnits="userSpaceOnUse"
                            colorInterpolationFilters="sRGB">
                        <feFlood floodOpacity="0" result="BackgroundImageFix"/>
                        <feBlend mode="normal" in="SourceGraphic" in2="BackgroundImageFix" result="shape"/>
                        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                                       result="hardAlpha"/>
                        <feOffset dx="2" dy="2"/>
                        <feGaussianBlur stdDeviation="5"/>
                        <feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
                        <feColorMatrix type="matrix"
                                       values="0 0 0 0 0.551597 0 0 0 0 0.58974 0 0 0 0 0.704167 0 0 0 0.2 0"/>
                        <feBlend mode="multiply" in2="shape" result="effect1_innerShadow_1_165"/>
                        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                                       result="hardAlpha"/>
                        <feOffset dx="2" dy="2"/>
                        <feGaussianBlur stdDeviation="3"/>
                        <feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
                        <feColorMatrix type="matrix"
                                       values="0 0 0 0 0.552941 0 0 0 0 0.588235 0 0 0 0 0.705882 0 0 0 0.31 0"/>
                        <feBlend mode="multiply" in2="effect1_innerShadow_1_165" result="effect2_innerShadow_1_165"/>
                        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                                       result="hardAlpha"/>
                        <feOffset dx="-2" dy="-2"/>
                        <feGaussianBlur stdDeviation="4"/>
                        <feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
                        <feColorMatrix type="matrix" values="0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0.5 0"/>
                        <feBlend mode="normal" in2="effect2_innerShadow_1_165" result="effect3_innerShadow_1_165"/>
                        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                                       result="hardAlpha"/>
                        <feOffset dx="-1" dy="-1"/>
                        <feGaussianBlur stdDeviation="7"/>
                        <feComposite in2="hardAlpha" operator="arithmetic" k2="-1" k3="1"/>
                        <feColorMatrix type="matrix" values="0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0.5 0"/>
                        <feBlend mode="normal" in2="effect3_innerShadow_1_165" result="effect4_innerShadow_1_165"/>
                    </filter>
                </defs>
            </svg>

            <svg
                className={styles.dayMonitorProcessBarElement}
                xmlns="http://www.w3.org/2000/svg" width="160" height="160" viewBox="0 0 160 160" fill="none">
                <g filter="url(#filter0_ddd_1_166)">
                    <circle cx="76" cy="76" r="50" fill="white"/>
                </g>
                <defs>
                    <filter id="filter0_ddd_1_166" x="0" y="0" width="160" height="160" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
                        <feFlood floodOpacity="0" result="BackgroundImageFix"/>
                        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                        <feOffset dx="4" dy="4"/>
                        <feGaussianBlur stdDeviation="15"/>
                        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.06 0"/>
                        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_1_166"/>
                        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                        <feOffset dx="-4" dy="-4"/>
                        <feGaussianBlur stdDeviation="4.5"/>
                        <feColorMatrix type="matrix" values="0 0 0 0 1 0 0 0 0 1 0 0 0 0 1 0 0 0 0.2 0"/>
                        <feBlend mode="normal" in2="effect1_dropShadow_1_166" result="effect2_dropShadow_1_166"/>
                        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                        <feOffset dx="2" dy="2"/>
                        <feGaussianBlur stdDeviation="3.5"/>
                        <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.07 0"/>
                        <feBlend mode="multiply" in2="effect2_dropShadow_1_166" result="effect3_dropShadow_1_166"/>
                        <feBlend mode="normal" in="SourceGraphic" in2="effect3_dropShadow_1_166" result="shape"/>
                    </filter>
                </defs>
            </svg>

            <svg
                className={styles.dayMonitorProcessBar}
                xmlns="http://www.w3.org/2000/svg" width="99" height="126" viewBox="0 0 99 126" fill="none">
                <path d="M63 122.85C63 124.59 61.5887 126.008 59.8512 125.921C50.9954 125.478 42.3201 123.17 34.3986 119.133C25.5402 114.62 17.8757 108.074 12.0319 100.03C6.18813 91.9872 2.33091 82.675 0.775633 72.8554C-0.779646 63.0357 0.0111768 52.9874 3.08344 43.5319C6.15571 34.0765 11.4222 25.4824 18.4523 18.4523C25.4824 11.4222 34.0765 6.1557 43.5319 3.08344C52.9874 0.0111732 63.0357 -0.779646 72.8554 0.775637C81.6365 2.16643 90.0118 5.398 97.4368 10.2448C98.8936 11.1958 99.2015 13.1729 98.179 14.5803C97.1564 15.9878 95.1909 16.2917 93.7289 15.3489C87.1122 11.0819 79.6688 8.23332 71.8699 6.99808C63.0322 5.59832 53.9887 6.31006 45.4787 9.0751C36.9688 11.8401 29.2341 16.58 22.9071 22.907C16.58 29.2341 11.8401 36.9688 9.0751 45.4787C6.31006 53.9886 5.59832 63.0321 6.99807 71.8698C8.39782 80.7075 11.8693 89.0885 17.1287 96.3274C22.3881 103.566 29.2861 109.458 37.2587 113.52C44.2943 117.105 51.9905 119.175 59.8515 119.613C61.5886 119.709 63 121.11 63 122.85Z" fill="url(#paint0_linear_1_167)"/>
                <defs>
                    <linearGradient id="paint0_linear_1_167" x1="43.1907" y1="-7.46907" x2="114.324" y2="48.1705" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#FA9C59"/>
                        <stop offset="1" stop-color="#FD7043"/>
                    </linearGradient>
                </defs>
            </svg>

            <svg
                className={styles.dayMonitorProcessBarNumberWrapper}
                xmlns="http://www.w3.org/2000/svg" width="100" height="100" viewBox="0 0 100 100" fill="none">
                <g filter="url(#filter0_d_1_169)">
                    <path d="M45 80C64.33 80 80 64.33 80 45C80 25.67 64.33 10 45 10C25.67 10 10 25.67 10 45C10 64.33 25.67 80 45 80Z" fill="url(#paint0_linear_1_169)"/>
                </g>
                <defs>
                    <filter id="filter0_d_1_169" x="0" y="0" width="100" height="100" filterUnits="userSpaceOnUse" color-interpolation-filters="sRGB">
                        <feFlood floodOpacity="0" result="BackgroundImageFix"/>
                        <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                        <feOffset dx="5" dy="5"/>
                        <feGaussianBlur stdDeviation="7.5"/>
                        <feColorMatrix type="matrix" values="0 0 0 0 0.992157 0 0 0 0 0.439216 0 0 0 0 0.262745 0 0 0 0.3 0"/>
                        <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_1_169"/>
                        <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_1_169" result="shape"/>
                    </filter>
                    <linearGradient id="paint0_linear_1_169" x1="42.4459" y1="10" x2="40.4233" y2="79.9415" gradientUnits="userSpaceOnUse">
                        <stop stop-color="#FA9C59"/>
                        <stop offset="1" stop-color="#FD7043"/>
                    </linearGradient>
                </defs>
            </svg>
            <p className={styles.dayMonitorProcessBarNumber}>365</p>
        </div>
    );
};

export default MyComponent;
