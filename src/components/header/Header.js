import React from 'react';

import styles from './Header.module.scss';

const Header = () => {
    return (
        <header className={styles.header}>
            <div className={styles.headerButtonWrapper}>
                <button className={styles.headerButton}>
                    Мониторинг
                </button>
                <svg xmlns="http://www.w3.org/2000/svg" width="5" height="19" viewBox="0 0 5 19" fill="none">
                    <rect x="5" y="12" width="5" height="5" rx="2.5" transform="rotate(180 5 12)"
                          fill="url(#paint0_linear_1_245)"/>
                    <defs>
                        <linearGradient id="paint0_linear_1_245" x1="7.31757" y1="12" x2="7.17309" y2="16.9958"
                                        gradientUnits="userSpaceOnUse">
                            <stop stop-color="#F9B27F"/>
                            <stop offset="1" stop-color="#F78D5F"/>
                        </linearGradient>
                    </defs>
                </svg>
                <button className={styles.headerButton}>
                    Обменные пункты
                </button>
                <svg xmlns="http://www.w3.org/2000/svg" width="5" height="19" viewBox="0 0 5 19" fill="none">
                    <rect x="5" y="12" width="5" height="5" rx="2.5" transform="rotate(180 5 12)"
                          fill="url(#paint0_linear_1_245)"/>
                    <defs>
                        <linearGradient id="paint0_linear_1_245" x1="7.31757" y1="12" x2="7.17309" y2="16.9958"
                                        gradientUnits="userSpaceOnUse">
                            <stop stop-color="#F9B27F"/>
                            <stop offset="1" stop-color="#F78D5F"/>
                        </linearGradient>
                    </defs>
                </svg>
                <button className={styles.headerButton}>
                    Партнерам
                </button>
                <svg xmlns="http://www.w3.org/2000/svg" width="5" height="19" viewBox="0 0 5 19" fill="none">
                    <rect x="5" y="12" width="5" height="5" rx="2.5" transform="rotate(180 5 12)"
                          fill="url(#paint0_linear_1_245)"/>
                    <defs>
                        <linearGradient id="paint0_linear_1_245" x1="7.31757" y1="12" x2="7.17309" y2="16.9958"
                                        gradientUnits="userSpaceOnUse">
                            <stop stop-color="#F9B27F"/>
                            <stop offset="1" stop-color="#F78D5F"/>
                        </linearGradient>
                    </defs>
                </svg>
                <button className={styles.headerButton}>
                    Помощь
                </button>
                <svg xmlns="http://www.w3.org/2000/svg" width="5" height="19" viewBox="0 0 5 19" fill="none">
                    <rect x="5" y="12" width="5" height="5" rx="2.5" transform="rotate(180 5 12)"
                          fill="url(#paint0_linear_1_245)"/>
                    <defs>
                        <linearGradient id="paint0_linear_1_245" x1="7.31757" y1="12" x2="7.17309" y2="16.9958"
                                        gradientUnits="userSpaceOnUse">
                            <stop stop-color="#F9B27F"/>
                            <stop offset="1" stop-color="#F78D5F"/>
                        </linearGradient>
                    </defs>
                </svg>
                <button className={styles.headerButton}>
                    О нас
                </button>
            </div>

            <div className={styles.headerRightContent}>
                <button className={styles.headerToggle}>
                    <svg
                        className={styles.headerToggleSvg}
                        width="42" height="24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <g filter="url(#filter0_d_1055_6646)">


                            <rect x="21" y="3" width="18" height="18" rx="9" fill="#F78D5F"/>
                        </g>
                        <defs>
                            <filter id="filter0_d_1055_6646" x="0" y="0" width="82" height="64" filterUnits="userSpaceOnUse"
                                    colorInterpolationFilters="sRGB">
                                <feFlood floodOpacity="0" result="BackgroundImageFix"/>
                                <feColorMatrix in="SourceAlpha" type="matrix"
                                               values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" result="hardAlpha"/>
                                <feOffset dy="4"/>
                                <feGaussianBlur stdDeviation="10"/>
                                <feComposite in2="hardAlpha" operator="out"/>
                                <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.06 0"/>
                                <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow_1055_6646"/>
                                <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow_1055_6646"
                                         result="shape"/>
                            </filter>
                        </defs>
                    </svg>
                </button>
                <div className={styles.headerLanguageWrapper}>
                    <p className={styles.headerLanguage}>RU</p>
                    <svg style={{marginLeft: '3px'}} xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15" fill="none">
                        <path fillRule="evenodd" clipRule="evenodd" d="M2.18761 5.27495C1.90622 5.60377 1.94589 6.09739 2.27622 6.37749L7.72769 11L12.7491 6.35503C13.067 6.06099 13.0852 5.56613 12.7898 5.24971C12.4944 4.9333 11.9973 4.91517 11.6794 5.2092L7.68226 8.90667L3.29524 5.18675C2.96491 4.90665 2.469 4.94614 2.18761 5.27495Z" fill="#3C3B65"/>
                    </svg>
                </div>
                <button className={styles.headerLoginButton}>
                    Клиент
                </button>
            </div>



        </header>
    );
};

export default Header;
