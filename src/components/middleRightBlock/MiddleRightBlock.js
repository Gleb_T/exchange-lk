import React from 'react';

import styles from './MiddleRightBlock.module.scss';
import DayMonitor from "@/components/userSetting/dayMonitor/DayMonitor";

const MiddleRightBlock = () => {
    return (
        <div className={styles.middleRightBlock}>
            <DayMonitor/>
            <div className={styles.middleRightBlockTextWrapper}>
                <h4 className={styles.middleRightBlockHeader}>Дней на сервисе</h4>
                <div className={styles.middleRightBlockTextContainer}>
                    <p className={styles.middleRightBlockText}>Дата регистрации: </p>
                    &nbsp;
                    <span className={styles.middleRightBlockNumber}>01.05.2023</span>
                </div>
                <div className={styles.middleRightBlockTextContainer}>
                    <p className={styles.middleRightBlockText}>Ваш уровень: </p>
                    &nbsp;
                    <span className={styles.middleRightBlockNumber}> Бывалый</span>
                </div>
                <div className={styles.middleRightBlockTextContainer}>
                    <p className={styles.middleRightBlockText}>До следующего статуса: </p>
                    &nbsp;
                    <span className={styles.middleRightBlockNumber}> 365 дней</span>
                </div>
            </div>
        </div>
    );
};

export default MiddleRightBlock;
