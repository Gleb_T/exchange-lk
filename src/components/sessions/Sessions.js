import React from 'react';

import styles from './Sessions.module.scss';

const Sessions = () => {
    return (
        <div className={styles.sessions}>
            <div className={styles.sessionsHeaderWrapper}>
                <p className={styles.sessionsNames}>Дата входа</p>
                <p className={styles.sessionsNames}>Дата завершения</p>
                <p className={styles.sessionsNames}>IP адрес</p>
                <p className={styles.sessionsNames}>ГЕО</p>
                <p className={styles.sessionsNames}>Устройство</p>
                <p className={styles.sessionsNames}>Браузер</p>
                <p className={styles.sessionsNames}>Вход с 2FA</p>
                <p className={styles.sessionsNames}>Наличие cookies</p>
                <div>
                    <button className={styles.sessionsButton}>
                        Завершить все
                    </button>
                </div>
            </div>
            <div className={styles.sessionsItemsWrapper}>
                <div className={styles.sessionsItems}>
                    <p className={styles.sessionsItemsText}>01.05.23 12:00</p>
                    <p className={styles.sessionsItemsText}>01.05.23 16:05</p>
                    <p className={styles.sessionsItemsText}>127.0.0.1</p>
                    <p className={styles.sessionsItemsText}>RU</p>
                    <p className={styles.sessionsItemsText}>Android 5.9</p>
                    <p className={styles.sessionsItemsText}>Chrome 10.5</p>
                    <p className={styles.sessionsItemsText}>Да</p>
                    <p className={styles.sessionsItemsText}>Да</p>
                    <div>
                        <button className={styles.sessionsItemButton}>
                            <p className={styles.sessionsItemButtonText}>Завершить сеанс</p>
                        </button>
                    </div>
                </div>
                <div className={styles.sessionsItems}>
                    <p className={styles.sessionsItemsText}>01.05.23 12:00</p>
                    <p className={styles.sessionsItemsText}>01.05.23 16:05</p>
                    <p className={styles.sessionsItemsText}>127.0.0.1</p>
                    <p className={styles.sessionsItemsText}>RU</p>
                    <p className={styles.sessionsItemsText}>Android 5.9</p>
                    <p className={styles.sessionsItemsText}>Chrome 10.5</p>
                    <p className={styles.sessionsItemsText}>Да</p>
                    <p className={styles.sessionsItemsText}>Да</p>
                    <button className={styles.sessionsItemButton}>
                        <p className={styles.sessionsItemButtonText}>Завершить сеанс</p>
                    </button>
                </div>
                <div className={styles.sessionsItems}>
                    <p className={styles.sessionsItemsText}>01.05.23 12:00</p>
                    <p className={styles.sessionsItemsText}>01.05.23 16:05</p>
                    <p className={styles.sessionsItemsText}>127.0.0.1</p>
                    <p className={styles.sessionsItemsText}>RU</p>
                    <p className={styles.sessionsItemsText}>Android 5.9</p>
                    <p className={styles.sessionsItemsText}>Chrome 10.5</p>
                    <p className={styles.sessionsItemsText}>Да</p>
                    <p className={styles.sessionsItemsText}>Да</p>
                    <button className={styles.sessionsItemButton}>
                        <p className={styles.sessionsItemButtonText}>Завершить сеанс</p>
                    </button>
                </div>
                <div className={styles.sessionsItems}>
                    <p className={styles.sessionsItemsText}>01.05.23 12:00</p>
                    <p className={styles.sessionsItemsText}>01.05.23 16:05</p>
                    <p className={styles.sessionsItemsText}>127.0.0.1</p>
                    <p className={styles.sessionsItemsText}>RU</p>
                    <p className={styles.sessionsItemsText}>Android 5.9</p>
                    <p className={styles.sessionsItemsText}>Chrome 10.5</p>
                    <p className={styles.sessionsItemsText}>Да</p>
                    <p className={styles.sessionsItemsText}>Да</p>
                    <button className={styles.sessionsItemButton}>
                        <p className={styles.sessionsItemButtonText}>Завершить сеанс</p>
                    </button>
                </div>
                <div className={styles.sessionsItems}>
                    <p className={styles.sessionsItemsText}>01.05.23 12:00</p>
                    <p className={styles.sessionsItemsText}>01.05.23 16:05</p>
                    <p className={styles.sessionsItemsText}>127.0.0.1</p>
                    <p className={styles.sessionsItemsText}>RU</p>
                    <p className={styles.sessionsItemsText}>Android 5.9</p>
                    <p className={styles.sessionsItemsText}>Chrome 10.5</p>
                    <p className={styles.sessionsItemsText}>Да</p>
                    <p className={styles.sessionsItemsText}>Да</p>
                    <button className={styles.sessionsItemButton}>
                        <p className={styles.sessionsItemButtonText}>Завершить сеанс</p>
                    </button>
                </div>
                <div className={styles.sessionsItems}>
                    <p className={styles.sessionsItemsText}>01.05.23 12:00</p>
                    <p className={styles.sessionsItemsText}>01.05.23 16:05</p>
                    <p className={styles.sessionsItemsText}>127.0.0.1</p>
                    <p className={styles.sessionsItemsText}>RU</p>
                    <p className={styles.sessionsItemsText}>Android 5.9</p>
                    <p className={styles.sessionsItemsText}>Chrome 10.5</p>
                    <p className={styles.sessionsItemsText}>Да</p>
                    <p className={styles.sessionsItemsText}>Да</p>
                    <button className={styles.sessionsItemButton}>
                        <p className={styles.sessionsItemButtonText}>Завершить сеанс</p>
                    </button>
                </div>
                <div className={styles.sessionsItems}>
                    <p className={styles.sessionsItemsText}>01.05.23 12:00</p>
                    <p className={styles.sessionsItemsText}>01.05.23 16:05</p>
                    <p className={styles.sessionsItemsText}>127.0.0.1</p>
                    <p className={styles.sessionsItemsText}>RU</p>
                    <p className={styles.sessionsItemsText}>Android 5.9</p>
                    <p className={styles.sessionsItemsText}>Chrome 10.5</p>
                    <p className={styles.sessionsItemsText}>Да</p>
                    <p className={styles.sessionsItemsText}>Да</p>
                    <button className={styles.sessionsItemButton}>
                        <p className={styles.sessionsItemButtonText}>Завершить сеанс</p>
                    </button>
                </div>
                <div className={styles.sessionsItems}>
                    <p className={styles.sessionsItemsText}>01.05.23 12:00</p>
                    <p className={styles.sessionsItemsText}>01.05.23 16:05</p>
                    <p className={styles.sessionsItemsText}>127.0.0.1</p>
                    <p className={styles.sessionsItemsText}>RU</p>
                    <p className={styles.sessionsItemsText}>Android 5.9</p>
                    <p className={styles.sessionsItemsText}>Chrome 10.5</p>
                    <p className={styles.sessionsItemsText}>Да</p>
                    <p className={styles.sessionsItemsText}>Да</p>
                    <button className={styles.sessionsItemButton}>
                        <p className={styles.sessionsItemButtonText}>Завершить сеанс</p>
                    </button>
                </div>
                <div className={styles.sessionsItems}>
                    <p className={styles.sessionsItemsText}>01.05.23 12:00</p>
                    <p className={styles.sessionsItemsText}>01.05.23 16:05</p>
                    <p className={styles.sessionsItemsText}>127.0.0.1</p>
                    <p className={styles.sessionsItemsText}>RU</p>
                    <p className={styles.sessionsItemsText}>Android 5.9</p>
                    <p className={styles.sessionsItemsText}>Chrome 10.5</p>
                    <p className={styles.sessionsItemsText}>Да</p>
                    <p className={styles.sessionsItemsText}>Да</p>
                    <button className={styles.sessionsItemButton}>
                        <p className={styles.sessionsItemButtonText}>Завершить сеанс</p>
                    </button>
                </div>
                <div className={styles.sessionsItems}>
                    <p className={styles.sessionsItemsText}>01.05.23 12:00</p>
                    <p className={styles.sessionsItemsText}>01.05.23 16:05</p>
                    <p className={styles.sessionsItemsText}>127.0.0.1</p>
                    <p className={styles.sessionsItemsText}>RU</p>
                    <p className={styles.sessionsItemsText}>Android 5.9</p>
                    <p className={styles.sessionsItemsText}>Chrome 10.5</p>
                    <p className={styles.sessionsItemsText}>Да</p>
                    <p className={styles.sessionsItemsText}>Да</p>
                    <button className={styles.sessionsItemButton}>
                        <p className={styles.sessionsItemButtonText}>Завершить сеанс</p>
                    </button>
                </div>
            </div>
        </div>
    );
};

export default Sessions;
