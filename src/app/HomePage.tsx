import React from "react";
import styles from './HomePage.module.scss';
import Aside from "@/components/Aside/Aside";
import Header from "@/components/header/Header";
import UserSetting from "@/components/userSetting/UserSetting";
import MiddleRightBlock from "@/components/middleRightBlock/MiddleRightBlock";
import Sessions from "@/components/sessions/Sessions";


//TODO:

//  адаптив на другие расширения кроме широкого 1440, 1920 (какие основные размеры?)
//  построение приложения (гл. переменные, где что переиспользуется, размеры адаптива, эссайд с какого размера убирается?)
//  какой layout
//  какое поведение элементов при наведении / нажатии , какие эффекты
//  сократить код, переменные вынести в отдельные файлы
//  доделать тоглы сразу с логикой
//  ProcessBarNumber бэк
//  мобильный вид дизайн
//  TS при подключении логики


const HomePage = () => {

    return (
        <main className={styles.mainHomePage}>
            <div className={styles.mainHomePageWrapper}>
                <section className={styles.mainHomePageLeftContainer}>
                    <Aside/>
                </section>
                <section className={styles.mainHomePageRightContainer}>
                    <Header/>
                    <div className={styles.mainHomePageMiddleContainer}>
                        <div className={styles.mainHomePageMiddleContent}>
                            <UserSetting/>
                        </div>
                        <div className={styles.mainHomePageMiddleContent}>
                            <MiddleRightBlock/>
                        </div>
                    </div>
                    <div className={styles.mainHomePageContent}>
                        <div className={styles.mainHomePageContentTextContainer}>
                            <div className={styles.mainHomePageContentTextWrapper}>
                                <svg
                                    className={styles.mainHomePageContentTextWrapperImage}
                                    xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15"
                                     fill="none">
                                    <g clipPath="url(#clip0_1_222)">
                                        <path
                                            d="M7.5 0C11.6423 0 15 3.35775 15 7.5C15 11.6423 11.6423 15 7.5 15C3.35775 15 0 11.6423 0 7.5H1.5C1.5 10.8135 4.1865 13.5 7.5 13.5C10.8135 13.5 13.5 10.8135 13.5 7.5C13.5 4.1865 10.8135 1.5 7.5 1.5C5.4375 1.5 3.618 2.54025 2.53875 4.125H4.5V5.625H0V1.125H1.5V3C2.868 1.1775 5.04675 0 7.5 0ZM8.25 3.75V7.18875L10.6823 9.621L9.621 10.6823L6.75 7.80975V3.75H8.25Z"
                                            fill="#3C3B65"/>
                                    </g>
                                    <defs>
                                        <clipPath id="clip0_1_222">
                                            <rect width="15" height="15" fill="white"/>
                                        </clipPath>
                                    </defs>
                                </svg>
                                <p className={styles.mainHomePageContentText}>История входов</p>
                            </div>
                            <div className={styles.mainHomePageContentTextWrapperActive}>
                                <svg
                                    className={styles.mainHomePageContentTextWrapperImage}
                                    xmlns="http://www.w3.org/2000/svg" width="15" height="15" viewBox="0 0 15 15"
                                     fill="none">
                                    <path
                                        d="M9 11.0526V12.5263L10.5 13.2632V14H4.5L4.49729 13.2658L6 12.5263V11.0526H0.74385C0.333038 11.0526 0 10.7219 0 10.3103V0.742354C0 0.332367 0.341483 0 0.74385 0H14.2561C14.667 0 15 0.330783 15 0.742354V10.3103C15 10.7202 14.6585 11.0526 14.2561 11.0526H9ZM1.5 8.10526V9.57895H13.5V8.10526H1.5Z"
                                        fill="#1DA0FF"/>
                                </svg>
                                <p className={styles.mainHomePageContentTextSelect}>Активные сессии</p>
                            </div>
                        </div>
                        <div className={styles.mainHomePageContentTextContainer}>
                            <h3 className={styles.mainHomePageContentHeader}>Активные сессии</h3>
                        </div>
                        <div className={styles.mainHomePageContentTextContainer}/>
                    </div>
                    <div className={styles.mainHomePageContentLine}/>
                    <Sessions  />
                </section>
            </div>
            <footer className={styles.mainHomePageFooter}>
                <p className={styles.mainHomePageFooterText}>
                    ExSun © 2023
                </p>
            </footer>
        </main>
    )
}

export default HomePage;
