import React from 'react';
import HomePage from "../app/HomePage";

const Home = () => {
    return (
        <HomePage />
    );
};

export default Home;